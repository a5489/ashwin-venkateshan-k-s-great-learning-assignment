package com.greatLearning.assignment;

public class HrDepartment extends SuperDepartment {
	@Override
	public String deparmentName() {
		
		return "HR Department";
		
	}
	@Override
	public String getTodaysWork() {
		
		return "Fill todays worksheet and mark your attendance";
	}
	@Override
	public String getWorkDeadline() {
	
		return "Complete by EOD";
	}
	
	public String doActivity() {
		return "Team Lunch";
	}
}
