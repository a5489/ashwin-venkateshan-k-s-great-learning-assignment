package com.greatLearning.assignment;
/* Version 1
public class Main {

	public static void main(String[] args) {
		AdminDepartment ad = new AdminDepartment();

		System.out.println("Department name: "+ad.deparmentName());
		
		System.out.println("Work: "+ad.getTodaysWork());
		
		System.out.println("Deadline: "+ad.getWorkDeadline());
		
		System.out.println("Holiday: "+ ad.isTodayAHoliday());
		
		System.out.println();
		
		HrDepartment hd = new HrDepartment();
		
		
		System.out.println("Department name: "+hd.deparmentName());
		
		System.out.println("Work: "+hd.getTodaysWork());
		
		System.out.println("Deadline: "+hd.getWorkDeadline());
		
		System.out.println("Activity: "+hd.doActivity());
		
		System.out.println("Holiday: "+ hd.isTodayAHoliday());
		
		System.out.println();
		
		TechDepartment td = new TechDepartment();
		
		
		System.out.println("Department name: "+td.deparmentName());
		
		System.out.println("Work: "+td.getTodaysWork());
		
		System.out.println("Deadline: "+td.getWorkDeadline());
		
		System.out.println("Stack Information: "+td.getTechStackInformation());
		
		System.out.println("Holiday: "+ td.isTodayAHoliday());
		

	}

}
*/
//Version 2
public class Main {

	public static void main(String[] args) {
		AdminDepartment ad = new AdminDepartment();

		System.out.println(ad.deparmentName());
		
		System.out.println(ad.getTodaysWork());
		
		System.out.println(ad.getWorkDeadline());
		
		System.out.println(ad.isTodayAHoliday());
		
		//System.out.println();
		
		HrDepartment hd = new HrDepartment();
		
		
		System.out.println(hd.deparmentName());
		
		System.out.println(hd.getTodaysWork());
		
		System.out.println(hd.getWorkDeadline());
		
		System.out.println(hd.doActivity());
		
		System.out.println(hd.isTodayAHoliday());
		
		//System.out.println();
		
		TechDepartment td = new TechDepartment();
		
		
		System.out.println(td.deparmentName());
		
		System.out.println(td.getTodaysWork());
		
		System.out.println(td.getWorkDeadline());
		
		System.out.println(td.getTechStackInformation());
		
		System.out.println(td.isTodayAHoliday());
		

	}
	

}

